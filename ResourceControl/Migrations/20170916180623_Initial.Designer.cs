﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using ResourceControl.DataAccess;

namespace ResourceControl.Migrations {
    [DbContext(typeof(ResourceControlContext))]
    [Migration("20170916180623_Initial")]
    partial class Initial {
        protected override void BuildTargetModel(ModelBuilder modelBuilder) {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ResourceControl.Models.Resource", 
                b => {
                    b.Property<long>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<string>("Description");

                    b.Property<string>("Observation");

                    b.Property<long>("Quantity");

                    b.HasKey("ID");

                    b.ToTable("Resource");
                });
        }
    }
}
