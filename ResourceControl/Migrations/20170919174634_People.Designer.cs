﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using ResourceControl.DataAccess;

namespace ResourceControl.Migrations
{
    [DbContext(typeof(ResourceControlContext))]
    [Migration("20170919174634_People")]
    partial class People
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ResourceControl.Models.Person", b =>
                {
                    b.Property<long>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(40);

                    b.HasKey("ID");

                    b.ToTable("Person");
                });

            modelBuilder.Entity("ResourceControl.Models.Resource", b =>
                {
                    b.Property<long>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .HasMaxLength(140);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(24);

                    b.Property<string>("Observation")
                        .HasMaxLength(40);

                    b.Property<long>("Quantity");

                    b.HasKey("ID");

                    b.ToTable("Resource");
                });

            modelBuilder.Entity("ResourceControl.Models.Transaction", b =>
                {
                    b.Property<long>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("PersonID");

                    b.Property<long>("Quantity");

                    b.Property<long>("ResourceID");

                    b.HasKey("ID");

                    b.HasIndex("PersonID");

                    b.HasIndex("ResourceID");

                    b.ToTable("Transaction");
                });

            modelBuilder.Entity("ResourceControl.Models.Transaction", b =>
                {
                    b.HasOne("ResourceControl.Models.Person", "Person")
                        .WithMany("Transactions")
                        .HasForeignKey("PersonID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ResourceControl.Models.Resource", "Resource")
                        .WithMany("Transactions")
                        .HasForeignKey("ResourceID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
