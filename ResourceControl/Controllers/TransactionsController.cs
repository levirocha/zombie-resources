using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ResourceControl.DataAccess;
using ResourceControl.Models;

namespace ResourceControl.Controllers {
    public class TransactionsController : Controller {
        private readonly ITransactionsDAO _transactionsDAO;
        private readonly IResourcesDAO _resourcesDAO;
        private readonly IPeopleDAO _personsDAO;

        public TransactionsController(ITransactionsDAO transactionsDAO, IResourcesDAO resourcesDAO, IPeopleDAO personsDAO) {
            _transactionsDAO = transactionsDAO;
            _resourcesDAO = resourcesDAO;
            _personsDAO = personsDAO;
        }

        // GET: Transactions
        public async Task<IActionResult> Index(string searchString) {
            return View(await _transactionsDAO.GetListAsync(searchString));
        }

        // GET: Transactions/Details/id
        // Currently unused
        public async Task<IActionResult> Details(long? id) {
            var transaction = await _transactionsDAO.GetById(id);
            if (transaction == null)
                return NotFound();
            return View(transaction);
        }

        // GET: Transactions/Create?ResourceID
        public IActionResult Create(long ResourceID) {
            ViewData["ResourceID"] = _resourcesDAO.SelectList(ResourceID);
            ViewData["PersonID"] = _personsDAO.SelectList();
            return View();
        }

        // POST: Transactions/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,ResourceID,PersonID,Quantity,Description")] Transaction transaction) {
            if (ModelState.IsValid) {
                await _transactionsDAO.Insert(transaction);
                return RedirectToAction("Index");
            }
            ViewData["ResourceID"] = _resourcesDAO.SelectList(transaction.ResourceID);
            ViewData["PersonID"] = _personsDAO.SelectList(transaction.PersonID);
            return View(transaction);
        }

        // GET: Transactions/Edit/id
        // Currently unused
        public async Task<IActionResult> Edit(long? id)
        {
            var transaction = await _transactionsDAO.GetById(id);
            if (transaction == null)
                return NotFound();
            ViewData["ResourceID"] = _resourcesDAO.SelectList(transaction.ResourceID);
            ViewData["PersonID"] = _personsDAO.SelectList(transaction.PersonID);
            return View(transaction);
        }

        // POST: Transactions/Edit/id
        // Currently unused
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("ID,ResourceID,PersonID,Quantity,Description")] Transaction transaction) {
            if (id != transaction.ID)
                return NotFound();

            if (ModelState.IsValid) {
                if (await _transactionsDAO.Update(transaction))
                    return RedirectToAction("Index");
                else
                    return NotFound();
            }
            ViewData["ResourceID"] = _resourcesDAO.SelectList(transaction.ResourceID);
            ViewData["PersonID"] = _personsDAO.SelectList(transaction.PersonID);
            return View(transaction);
        }

        // GET: Transactions/Delete/id
        // Currently unused
        public async Task<IActionResult> Delete(long? id) {
            var transaction = await _transactionsDAO.GetById(id);
            if (transaction == null)
                return NotFound();
            return View(transaction);
        }

        // POST: Transactions/Delete/id
        // Currently unused
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id) {
            await _transactionsDAO.Delete(id);
            return RedirectToAction("Index");
        }
    }
}
