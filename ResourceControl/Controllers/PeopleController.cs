using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ResourceControl.DataAccess;
using ResourceControl.Models;

namespace ResourceControl.Controllers {
    public class PeopleController : Controller {
        private readonly IPeopleDAO _personsDAO;

        public PeopleController(IPeopleDAO personsDAO) {
            _personsDAO = personsDAO;    
        }

        // GET: People?searchString
        public async Task<IActionResult> Index(string searchString) {
            return View(await _personsDAO.GetListAsync(searchString));
        }

        // GET: People/Details/id
        public async Task<IActionResult> Details(long? id) {
            var person = await _personsDAO.GetById(id);
            if (person == null)
                return NotFound();
            return View(person);
        }

        // GET: People/Create
        public IActionResult Create() {
            return View();
        }

        // POST: People/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name")] Person person) {
            if (ModelState.IsValid) {
                await _personsDAO.Insert(person);
                return RedirectToAction("Index");
            }
            return View(person);
        }

        // GET: People/Edit/id
        public async Task<IActionResult> Edit(long? id) {
            var person = await _personsDAO.GetById(id);
            if (person == null)
                return NotFound();
            return View(person);
        }

        // POST: People/Edit/id
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("ID,Name")] Person person) {
            if (id != person.ID) {
                return NotFound();
            }

            if (ModelState.IsValid) {
                if (await _personsDAO.Update(person))
                    return RedirectToAction("Index");
                else
                    return NotFound();
            }
            return View(person);
        }

        // GET: People/Delete/id
        public async Task<IActionResult> Delete(long? id){
            var person = await _personsDAO.GetById(id);
            if (person == null)
                return NotFound();
            return View(person);
        }

        // POST: People/Delete/id
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id) {
            await _personsDAO.Delete(id);
            return RedirectToAction("Index");
        }

        [AcceptVerbs("Get", "Post")]
        public async Task<IActionResult> VerifyName(string name) {
            if (await _personsDAO.NameExists(name))
                return Json(data: $"Name {name} is already in use.");
            return Json(data: true);
        }
    }
}
