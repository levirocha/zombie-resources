using Microsoft.AspNetCore.Mvc;
using ResourceControl.DataAccess;
using ResourceControl.Models;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ResourceControl.Controllers {
    public class ResourcesController : Controller {
        private readonly IResourcesDAO _resourcesDAO;

        public ResourcesController(IResourcesDAO resourcesDAO) {
            _resourcesDAO = resourcesDAO;
        }

        // GET: Resources?searchString
        public async Task<IActionResult> Index(string searchString) {
            return View(await _resourcesDAO.GetListAsync(searchString));
        }

        // GET: Resources/Details/id
        public async Task<IActionResult> Details(long? id) {
            var resource = await _resourcesDAO.GetById(id);
            if (resource == null)
                return NotFound();
            return View(resource);
        }

        // GET: Resources/Create
        public IActionResult Create() {
            return View();
        }

        // POST: Resources/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Description,Observation,Quantity")] Resource resource) {
            if (ModelState.IsValid) {
                await _resourcesDAO.Insert(resource);
                return RedirectToAction("Index");
            }
            return View(resource);
        }

        // GET: Resources/Edit/id
        public async Task<IActionResult> Edit(long? id) {
            var resource = await _resourcesDAO.GetById(id);
            if (resource == null)
                return NotFound();
            return View(resource);
        }

        // POST: Resources/Edit/id
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("ID,Name,Description,Observation,Quantity")] Resource resource) {
            if (id != resource.ID) {
                return NotFound();
            }

            if (ModelState.IsValid) {
                if (await _resourcesDAO.Update(resource))
                    return RedirectToAction("Index");
                else
                    return NotFound();
            }
            return View(resource);
        }

        // GET: Resources/Delete/id
        public async Task<IActionResult> Delete(long? id) {
            var resource = await _resourcesDAO.GetById(id);
            if (resource == null)
                return NotFound();
            return View(resource);
        }

        // POST: Resources/Delete/id
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id) {
            await _resourcesDAO.Delete(id);
            return RedirectToAction("Index");
        }

        [AcceptVerbs("Get", "Post")]
        public async Task<IActionResult> VerifyName(string name) {
            if (await _resourcesDAO.NameExists(name))
                return Json(data: $"Name {name} is already in use.");
            return Json(data: true);
        }
    }
}
