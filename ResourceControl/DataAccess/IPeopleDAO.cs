﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ResourceControl.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ResourceControl.DataAccess {
    public interface IPeopleDAO {
        Task<List<Person>> GetListAsync(string searchString);
        Task<Person> GetById(long? id);
        Task<int> Insert(Person person);
        Task<bool> Update(Person person);
        Task<int> Delete(long id);
        SelectList SelectList(long? selectedID = null);
        Task<bool> NameExists(string name);
    }
}
