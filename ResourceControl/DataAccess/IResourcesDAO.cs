﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ResourceControl.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ResourceControl.DataAccess {
    public interface IResourcesDAO {
        Task<List<Resource>> GetListAsync(string searchString);
        Task<Resource> GetById(long? id);
        Task<int> Insert(Resource resource);
        Task<bool> Update(Resource resource);
        Task<int> Delete(long id);
        SelectList SelectList(long? selectedID = null);
        Task<bool> NameExists(string name);
    }
}
