﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ResourceControl.Models;

namespace ResourceControl.DataAccess {
    public interface ITransactionsDAO {
        Task<List<Transaction>> GetListAsync(string searchString);
        Task<Transaction> GetById(long? id);
        Task<int> Insert(Transaction resource);
        Task<bool> Update(Transaction resource);
        Task<int> Delete(long id);
    }
}
