﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ResourceControl.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResourceControl.DataAccess {
    public class PeopleDAO : IPeopleDAO {
        private readonly ResourceControlContext _context;

        public PeopleDAO(ResourceControlContext context) {
            _context = context;
        }

        public async Task<List<Person>> GetListAsync(string searchString) {
            var persons = from p in _context.Person select p;
            if (!String.IsNullOrEmpty(searchString))
                persons = persons.Where(p => p.Name.Contains(searchString));
            return await persons.ToListAsync();
        }

        public async Task<Person> GetById(long? id) {
            if (id == null)
                return null;
            return await _context.Person
                .Include(p => p.Transactions).ThenInclude(t => t.Resource)
                .AsNoTracking().SingleOrDefaultAsync(p => p.ID == id);
        }

        public async Task<int> Insert(Person person) {
            _context.Add(person);
            return await _context.SaveChangesAsync();
        }

        public async Task<bool> Update(Person person) {
            try {
                _context.Update(person);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException) {
                return false;
            }
            return true;
        }

        public async Task<int> Delete(long id) {
            var person = await _context.Person.SingleOrDefaultAsync(p => p.ID == id);
            _context.Person.Remove(person);
            return await _context.SaveChangesAsync();
        }

        public SelectList SelectList(long? selectedID = null) {
            if (selectedID == null)
                return new SelectList(_context.Person, "ID", "Name");
            else
                return new SelectList(_context.Person, "ID", "Name", selectedID);
        }

        public async Task<bool> NameExists(string name) {
            var persons = from p in _context.Person select p;
            persons = persons.Where(p => p.Name.Equals(name));
            var list = await persons.ToListAsync();
            if (list.Count > 0)
                return true;
            else
                return false;
        }
    }
}
