﻿using Microsoft.EntityFrameworkCore;

namespace ResourceControl.DataAccess
{
    public class ResourceControlContext : DbContext {
        public ResourceControlContext(DbContextOptions<ResourceControlContext> options)
            : base(options) { }

        public DbSet<ResourceControl.Models.Resource> Resource { get; set; }
        public DbSet<ResourceControl.Models.Transaction> Transaction { get; set; }
        public DbSet<ResourceControl.Models.Person> Person { get; set; }
    }
}
