﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ResourceControl.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResourceControl.DataAccess {
    public class ResourcesDAO : IResourcesDAO {
        private readonly ResourceControlContext _context;

        public ResourcesDAO(ResourceControlContext context) {
            _context = context;
        }

        public async Task<List<Resource>> GetListAsync(string searchString) {
            var resources = from r in _context.Resource select r;
            if (!String.IsNullOrEmpty(searchString))
                resources = resources.Where(s => s.Name.Contains(searchString) || s.Description.Contains(searchString) || s.Observation.Contains(searchString));
            return await resources.ToListAsync();
        }

        public async Task<Resource> GetById(long? id) {
            if (id == null)
                return null;
            return await _context.Resource
                .Include(r => r.Transactions).ThenInclude(t => t.Person)
                .AsNoTracking().SingleOrDefaultAsync(m => m.ID == id);
        }

        public async Task<int> Insert(Resource resource) {
            _context.Add(resource);
            return await _context.SaveChangesAsync();
        }

        public async Task<bool> Update(Resource resource) {
            try {
                _context.Update(resource);
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException) {
                return false;
            }
            return true;
        }

        public async Task<int> Delete(long id) {
            var resource = await _context.Resource.SingleOrDefaultAsync(m => m.ID == id);
            _context.Resource.Remove(resource);
            return await _context.SaveChangesAsync();
        }

        public SelectList SelectList(long? selectedID = null) {
            if (selectedID == null)
                return new SelectList(_context.Resource, "ID", "Name");
            else
                return new SelectList(_context.Resource, "ID", "Name", selectedID);
        }

        public async Task<bool> NameExists(string name) {
            var resources = from r in _context.Resource select r;
            resources = resources.Where(r => r.Name.Equals(name));
            var list = await resources.ToListAsync();
            if (list.Count > 0)
                return true;
            else
                return false;
        }

    }
}
