﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ResourceControl.Models;

namespace ResourceControl.DataAccess {
    public class TransactionsDAO : ITransactionsDAO {
        private readonly ResourceControlContext _context;

        public TransactionsDAO(ResourceControlContext context) {
            _context = context;
        }

        public async Task<List<Transaction>> GetListAsync(string searchString) {
            var transactions = from t in _context.Transaction select t;
            transactions = transactions.Include(t => t.Resource).Include(t => t.Person).AsNoTracking();
            if (!String.IsNullOrEmpty(searchString))
                transactions = transactions.Where(t => t.Resource.Name.Contains(searchString) || t.Person.Name.Contains(searchString));
            return await transactions.ToListAsync();
        }

        public async Task<Transaction> GetById(long? id) {
            if (id == null)
                return null;
            return await _context.Transaction
                .Include(t => t.Resource).Include(t => t.Person)
                .AsNoTracking().SingleOrDefaultAsync(t => t.ID == id);
        }

        public async Task<int> Insert(Transaction transaction) {
            _context.Add(transaction);
            // Update resource quantity
            Resource resource = await _context.Resource.SingleOrDefaultAsync(m => m.ID == transaction.ResourceID);
            resource.Quantity += transaction.Quantity;
            _context.Update(resource);
            return await _context.SaveChangesAsync();
        }

        public async Task<bool> Update(Transaction resource) {
            try {
                _context.Update(resource);
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException) {
                return false;
            }
            return true;
        }

        public async Task<int> Delete(long id) {
            var transaction = await _context.Transaction.SingleOrDefaultAsync(m => m.ID == id);
            _context.Transaction.Remove(transaction);
            return await _context.SaveChangesAsync();
        }

    }
}
