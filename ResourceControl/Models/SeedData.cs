﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ResourceControl.DataAccess;
using System;
using System.Linq;

namespace ResourceControl.Models {
    public class SeedData {
        public static void Initialize(IServiceProvider serviceProvider) {
            using (var context = new ResourceControlContext(
                serviceProvider.GetRequiredService<DbContextOptions<ResourceControlContext>>())) {
                
                if (context.Resource.Any()) {
                    return;   // DB already has data
                }

                context.Resource.AddRange(
                     new Resource
                     {
                         ID = 1,
                         Name = "Water bottle",
                         Description = "Plastic bottle with drinking water.",
                         Observation = "500ml",
                         Quantity = 4000
                     },

                     new Resource
                     {
                         ID = 2,
                         Name = "Sleeping bag",
                         Description = "Bag for sleeping.",
                         Observation = "Fits one person.",
                         Quantity = 30
                     },

                     new Resource
                     {
                         ID = 3,
                         Name = "AK-47",
                         Description = "Reliable gun.",
                         Observation = "Uses 7.62x39mm cartridges.",
                         Quantity = 7
                     },

                     new Resource
                     {
                         ID = 4,
                         Name = "7.62x39mm cartridges",
                         Description = "Ammo for assault rifles.",
                         Observation = "Each cartridge holds 39 bullets.",
                         Quantity = 300
                     },
                     
                     new Resource
                     {
                         ID = 5,
                         Name = "Rations bag",
                         Description = "Packed with nutrients",
                         Observation = "1 bag feeds 1 adult for 1 day.",
                         Quantity = 200
                     }
                     
                );
            }
        }
    }
}
