﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ResourceControl.Models {
    public class Person {
        public long ID { get; set; }

        [StringLength(40, MinimumLength = 2, ErrorMessage = "Name must be between 2 and 40 characters long.")]
        [Required]
        [Remote(action: "VerifyName", controller: "People")]
        public string Name { get; set; }

        public ICollection<Transaction> Transactions { get; set; }
    }
}
