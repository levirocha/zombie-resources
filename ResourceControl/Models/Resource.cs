﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ResourceControl.Models
{
    public class Resource
    {
        public long ID { get; set; }

        [StringLength(24, MinimumLength = 2, ErrorMessage = "Name must be between 2 and 24 characters long.")]
        [Required]
        [Remote(action: "VerifyName", controller: "Resources")]
        public string Name { get; set; }

        [StringLength(140, ErrorMessage = "Description must be shorter than 140 characters!")]
        public string Description { get; set; }

        [StringLength(40, ErrorMessage = "Observation must be shorter than 40 characters!")]
        public string Observation { get; set; }

        [DisplayFormat(DataFormatString = "{0:### ### ###}")]
        public long Quantity { get; set; }

        public ICollection<Transaction> Transactions { get; set; }

        // Start quantity at 0
        public Resource() { Quantity = 0; }
    }
}
