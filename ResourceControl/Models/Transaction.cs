﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace ResourceControl.Models {
    public class Transaction {
        public long ID { get; set; }
        [DisplayName("Resource")]
        public long ResourceID { get; set; }
        [DisplayName("Person")]
        public long PersonID { get; set; }

        [DisplayFormat(DataFormatString = "{0:### ### ###}")]
        [Required]
        public long Quantity { get; set; }

        [StringLength(140, ErrorMessage = "Description must be shorter than 140 characters!")]
        public string Description { get; set; }

        public Resource Resource { get; set; }
        public Person Person { get; set; }
    }
}
